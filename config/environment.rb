require 'bundler/setup'
require 'hanami/setup'
require 'hanami/model'
require_relative '../lib/shorty'
require_relative '../apps/api/application'
require_relative '../apps/api/errors'

Hanami.configure do
  mount Api::Application, at: '/api'

  model do
    adapter :sql, ENV.fetch('DATABASE_URL')
    migrations 'db/migrations'
    schema     'db/schema.sql'
  end
end
