require 'spec_helper'

describe Api::Serializers::CreateShortCode do
  subject { described_class.call(short_code) }
  let(:short_code) {
    ShortCode.new(
      original_url: Faker::Internet.url,
      target_url: random_short_code,
    )
  }

  it 'should serialize default attributes' do
    expect(subject).to have_key(:shortcode)
  end

  it 'should return shortcode' do
    expect(subject[:shortcode]).to eq(short_code.target_url)
  end
end
