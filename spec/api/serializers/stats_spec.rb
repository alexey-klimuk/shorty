require 'spec_helper'

describe Api::Serializers::ShortCodeStats do
  subject { described_class.call(short_code) }
  let(:short_code) {
    ShortCode.new(
      original_url: Faker::Internet.url,
      target_url: random_short_code,
      redirect_count: 0,
      created_at: Time.now
    )
  }

  it 'should serialize default attributes' do
    expect(subject).to have_key(:startDate)
    expect(subject).to have_key(:redirectCount)
    expect(subject).to_not have_key(:lastSeenDate)
  end

  it 'should return dates in ISO8601' do
    expect(subject[:startDate]).to eq(short_code.created_at.iso8601)
  end

  context 'when short_code redirects count > 0' do
    let(:short_code) {
      ShortCode.new(
        original_url: Faker::Internet.url,
        target_url: random_short_code,
        created_at: Time.now,
        updated_at: Time.now,
        redirect_count: 2
      )
    }

    it 'should serialize lastSeenDate attribute' do
      expect(subject).to have_key(:lastSeenDate)
    end
  end
end
