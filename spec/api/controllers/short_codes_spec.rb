require 'features_helper'

describe 'API short codes' do
  include Rack::Test::Methods

  def app
    Hanami.app
  end

  describe '[POST] /api/shorten' do
    context 'with valid params' do
      let(:valid_params) { { url: Faker::Internet.url, shortcode: random_short_code } }
      let(:expected_response) { { 'shortcode' => valid_params[:shortcode] } }

      it 'responses with 201' do
        expect_call(Api::Commands::CreateShortCode, valid_params)
        create_short_code(valid_params, 201, expected_response)
      end
    end


    context 'with missing shortcode' do
      let(:valid_params) { { url: Faker::Internet.url, shortcode: nil } }
      let(:expected_response) { { 'shortcode' => '123456' } }

      it 'responses with 201' do
        allow(SecureRandom).to receive(:hex).and_return('123456')
        expect_call(Api::Commands::CreateShortCode, valid_params)
        create_short_code(valid_params, 201, expected_response)
      end
    end

    context 'with missing url' do
      let(:invalid_params) { { url: nil, shortcode: random_short_code } }
      let(:expected_response) { { 'errors' => 'url is not present' } }

      it 'responses with 400' do
        expect_call(Api::Commands::CreateShortCode, invalid_params)
        create_short_code(invalid_params, 400, expected_response)
      end
    end

    context 'with invalid short code' do
      let(:invalid_params) { { url: Faker::Internet.url, shortcode: 'abcdefgh' } }
      let(:expected_response) { { 'errors' => 'The shortcode fails to meet the following regexp: ^[0-9a-zA-Z_]{4,}$.' } }

      it 'responses with 422' do
        expect_call(Api::Commands::CreateShortCode, invalid_params)
        create_short_code(invalid_params, 422, expected_response)
      end
    end

    context 'with duplicated short code' do
      let(:valid_params) { { url: Faker::Internet.url, shortcode: random_short_code } }
      let(:expected_response) { { 'errors' => 'The the desired shortcode is already in use. Shortcodes are case-sensitive.' } }

      it 'responses with 409' do
        post '/api/shorten', valid_params
        expect_call(Api::Commands::CreateShortCode, valid_params)
        create_short_code(valid_params, 409, expected_response)
      end
    end

    context 'with unexpected error' do
      let(:valid_params) { { url: Faker::Internet.url, shortcode: random_short_code } }
      let(:expected_response) { { 'errors' => 'Something went wrong' } }

      it 'responses with 409' do
        allow(Api::Commands::CreateShortCode).to receive(:validate_params!).and_raise(StandardError, 'Something went wrong')
        expect_call(Api::Commands::CreateShortCode, valid_params)
        create_short_code(valid_params, 500, expected_response)
      end
    end
  end

  describe '[GET] /api/:shortcode' do
    context 'when shortcode exists' do
      before(:all) do
        @valid_params = { url: Faker::Internet.url, shortcode: random_short_code }
        post '/api/shorten', @valid_params
      end

      it 'responses with 302' do
        get "/api/#{@valid_params[:shortcode]}"

        expect(last_response.status).to eq 302
      end


      it 'sets location header' do
        get "/api/#{@valid_params[:shortcode]}"

        expect(last_response.headers['Location']).to eq @valid_params[:url]
      end
    end

    context 'when shortcode does not exist' do
      it 'responses with 404' do
        get '/api/123456'

        expect(last_response.status).to eq 404
      end
    end

    context 'when something goes wrong' do
      let(:expected_response) { { 'errors' => 'Something went wrong' } }

      it 'responses with 500' do
        allow(Api::Queries::GetShortCode).to receive(:call).and_raise(StandardError, expected_response['errors'])

        get '/api/123456'

        expect(last_response.status).to eq 500
        expect(JSON.parse(last_response.body)).to eq expected_response
      end
    end
  end

  describe '[GET] /api/:shortcode/stats' do
    context 'when shortcode exists' do
      before(:all) do
        @valid_params = { url: Faker::Internet.url, shortcode: random_short_code }
        post '/api/shorten', @valid_params
      end

      it 'responses with 302' do
        get "/api/#{@valid_params[:shortcode]}"
        get "/api/#{@valid_params[:shortcode]}"
        get "/api/#{@valid_params[:shortcode]}/stats"

        short_code = ShortCodeRepository.new.find_by_target_url(@valid_params[:shortcode])

        expect(last_response.status).to eq 200
        expect(last_response.body).to eq Api::Serializers::ShortCodeStats.call(short_code).to_json
      end
    end

    context 'when shortcode does not exist' do
      it 'responses with 404' do
        get '/api/123456/stats'

        expect(last_response.status).to eq 404
      end
    end

    context 'when something goes wrong' do
      let(:expected_response) { { 'errors' => 'Something went wrong' } }

      it 'responses with 500' do
        allow(Api::Queries::GetShortCodeStats).to receive(:call).and_raise(StandardError, expected_response['errors'])

        get '/api/123456/stats'

        expect(last_response.status).to eq 500
        expect(JSON.parse(last_response.body)).to eq expected_response
      end
    end
  end

  private

  def expect_call(action, params)
    expect(action).to receive(:call).with(params).and_call_original
  end

  def create_short_code(params, status, expected_response)
    post '/api/shorten', params

    expect(last_response.status).to           eq status
    expect(JSON.parse(last_response.body)).to eq expected_response
  end
end
