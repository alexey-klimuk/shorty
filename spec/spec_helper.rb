require 'database_cleaner'
require 'simplecov'

SimpleCov.start if ENV['COVERAGE']

ENV['HANAMI_ENV'] ||= 'test'

require_relative '../config/environment'

Hanami.boot


RSpec.configure do |config|
  DatabaseCleaner.allow_remote_database_url = true
  config.before(:suite) do
    DatabaseCleaner.clean_with :deletion
  end

  config.before(:each) do
    DatabaseCleaner.strategy = :transaction
  end

  config.before(:each) do
    DatabaseCleaner.start
  end

  config.after(:each) do
    DatabaseCleaner.clean
  end
end

def random_short_code
  Faker::Base.regexify(/^[0-9a-zA-Z_]{6}$/)
end
