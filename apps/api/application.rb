# frozen_string_literal: true

require 'hanami/helpers'

module Api
  module Helpers
    def render_errors(errors)
      { errors: errors }.to_json
    end

    def render_result(data)
      data.to_json
    end
  end

  class Application < Hanami::Application
    configure do
      root __dir__

      body_parsers :json

      default_response_format :json

      default_request_format :json

      load_paths << %w[
        controllers commands serializers queries
      ]

      controller.prepare do
        include Api::Helpers
      end

      routes 'routes'

      security.x_frame_options 'DENY'

      security.x_content_type_options 'nosniff'

      security.x_xss_protection '1; mode=block'

      security.content_security_policy %(
        form-action 'self';
        frame-ancestors 'self';
        base-uri 'self';
        default-src 'none';
        script-src 'self';
        connect-src 'self';
        img-src 'self' https: data:;
        style-src 'self' 'unsafe-inline' https:;
        font-src 'self';
        object-src 'none';
        plugin-types application/pdf;
        child-src 'self';
        frame-src 'self';
        media-src 'self'
      )
    end

    configure :development do
    end

    configure :test do
    end

    configure :production do
    end
  end
end
