module Api
  module Errors
    Exception            = Class.new(StandardError)
    BadRequest           = Class.new(StandardError)
    ResourceNotFound     = Class.new(StandardError)
    Conflict             = Class.new(StandardError)
    ValueNotUnique       = Class.new(StandardError)
  end
end
