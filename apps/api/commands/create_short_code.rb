# frozen_string_literal: true

module Api
  module Commands
    module CreateShortCode
      module_function

      def call(params)
        ensure_shortcode(params)
        validate_params!(params)
        short_code = create_short_code(params)
        generate_result(short_code)
      end

      # ------------private-----------------

      def create_short_code(params)
        ShortCodeRepository.new.create(
          original_url: params[:url],
          target_url: params[:shortcode]
        )
      end

      def ensure_shortcode(params)
        params[:shortcode] ||= generate_random_short_code
      end

      def generate_random_short_code
        short_code = nil
        loop do
          short_code = SecureRandom.hex(3)
          break unless short_code_exists?(short_code)
        end
        short_code
      end

      def generate_result(short_code)
        Api::Serializers::CreateShortCode.call(short_code)
      end

      def validate_params!(params)
        if params[:url].nil?
          raise(
            Api::Errors::BadRequest,
            'url is not present'
          )
        end

        if short_code_invalid?(params[:shortcode])
          raise(
            Api::Errors::Conflict,
            'The shortcode fails to meet the following regexp: ^[0-9a-zA-Z_]{4,}$.'
          )
        end

        if short_code_exists?(params[:shortcode])
          raise(
            Api::Errors::ValueNotUnique,
            'The the desired shortcode is already in use. Shortcodes are case-sensitive.'
          )
        end
      end

      def short_code_exists?(short_code)
        !ShortCodeRepository.new.find_by_target_url(short_code).nil?
      end

      def short_code_invalid?(short_code)
        (/^[0-9a-zA-Z_]{6}$/ =~ short_code).nil?
      end

      private_class_method :create_short_code
      private_class_method :ensure_shortcode
      private_class_method :generate_random_short_code
      private_class_method :generate_result
      private_class_method :validate_params!
      private_class_method :short_code_exists?
      private_class_method :short_code_invalid?
    end
  end
end
