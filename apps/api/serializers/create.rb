module Api
  module Serializers
    module CreateShortCode
      module_function

      def call(short_code)
        {
          shortcode: short_code.target_url
        }
      end
    end
  end
end
