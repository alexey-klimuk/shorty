module Api
  module Serializers
    module ShortCodeStats
      module_function

      def call(short_code)
        redirect_count = short_code.redirect_count
        {
          startDate: short_code.created_at.iso8601,
          lastSeenDate: (short_code.updated_at.iso8601 if redirect_count > 0),
          redirectCount: redirect_count
        }.compact
      end
    end
  end
end
