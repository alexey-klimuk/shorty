# frozen_string_literal: true

module Api
  module Queries
    module GetShortCode
      module_function

      def call(params, increment: false)
        short_code = get_short_code(params)
        increment_counter(short_code) if increment
        short_code
      end

      # ------------private-----------------

      def get_short_code(params)
        result = ShortCodeRepository.new.find_by_target_url(params[:shortcode])
        if result.nil?
          raise(
            Api::Errors::ResourceNotFound,
            'The shortcode cannot be found in the system'
          )
        end
        result
      end

      def increment_counter(short_code)
        ShortCodeRepository.new.update(
          short_code.id,
          redirect_count: short_code.redirect_count + 1
        )
      end

      private_class_method :get_short_code
      private_class_method :increment_counter
    end
  end
end
