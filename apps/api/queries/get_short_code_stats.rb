# frozen_string_literal: true

module Api
  module Queries
    module GetShortCodeStats
      module_function

      def call(params)
        short_code = GetShortCode.call(params, increment: false)
        generate_result(short_code)
      end

      # ------------private-----------------

      def generate_result(short_code)
        Api::Serializers::ShortCodeStats.call(short_code)
      end

      private_class_method :generate_result
    end
  end
end
