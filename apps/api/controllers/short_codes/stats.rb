module Api::Controllers::ShortCodes
  class Stats
    include Api::Action

    def call(params)
      get_short_code(params.to_h)
    end

    private

    def get_short_code(params)
      result = Api::Queries::GetShortCodeStats.call(params)
      status(200, render_result(result))
    rescue Api::Errors::ResourceNotFound => error
      status(404, render_errors(error.message))
    rescue => error
      status(500, render_errors(error.message))
    end
  end
end
