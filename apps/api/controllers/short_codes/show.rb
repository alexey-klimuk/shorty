module Api::Controllers::ShortCodes
  class Show
    include Api::Action

    def call(params)
      get_short_code(params.to_h)
    end

    private

    def get_short_code(params)
      result = Api::Queries::GetShortCode.call(params, increment: true)
      self.headers.merge!({ 'Location' => result.original_url })
      status(302, render_result({}))
    rescue Api::Errors::ResourceNotFound => error
      status(404, render_errors(error.message))
    rescue => error
      status(500, render_errors(error.message))
    end
  end
end
