module Api::Controllers::ShortCodes
  class Create
    include Api::Action

    params do
      optional(:url).filled
      optional(:shortcode).filled
    end

    def call(params)
      create_short_code(params.to_h)
    end

    private

    def create_short_code(params)
      result = Api::Commands::CreateShortCode.call(params)
      status(201, render_result(result))
    rescue Api::Errors::BadRequest => error
      status(400, render_errors(error.message))
    rescue Api::Errors::ValueNotUnique => error
      status(409, render_errors(error.message))
    rescue Api::Errors::Conflict => error
      status(422, render_errors(error.message))
    rescue => error
      status(500, render_errors(error.message))
    end
  end
end
