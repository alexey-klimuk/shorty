# frozen_string_literal: true

post '/shorten',          to: 'short_codes#create'
get  '/:shortcode',       to: 'short_codes#show'
get  '/:shortcode/stats', to: 'short_codes#stats'
