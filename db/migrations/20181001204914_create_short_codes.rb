Hanami::Model.migration do
  change do
    create_table :short_codes do
      primary_key :id

      column :original_url, String
      column :target_url,   String

      column :created_at, DateTime, null: false
      column :updated_at, DateTime, null: false

      index :target_url, unique: true
      index :original_url
    end
  end
end
