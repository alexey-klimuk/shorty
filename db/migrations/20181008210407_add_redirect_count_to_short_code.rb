Hanami::Model.migration do
  change do
    alter_table :short_codes do
      add_column :redirect_count, Integer, default: 0
    end
  end
end
