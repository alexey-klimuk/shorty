# frozen_string_literal: true

source 'https://rubygems.org'

gem 'rake'
gem 'hanami',       '~> 1.2'
gem 'hanami-model', '~> 1.2'

gem 'pg'

gem 'puma'
gem 'rack-cors', require: 'rack/cors'

group :development do
  # Code reloading
  # See: http://hanamirb.org/guides/projects/code-reloading
  gem 'shotgun', platforms: :ruby
  gem 'hanami-webconsole'
end

group :test, :development do
  gem 'dotenv', '~> 2.0'
  gem 'rubocop', require: false
end

group :test do
  gem 'rspec'
  gem 'database_cleaner'
  gem 'webmock'
  gem 'rack-test'
  gem 'simplecov', require: false
  gem 'faker'
end
