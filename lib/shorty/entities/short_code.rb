class ShortCode < Hanami::Entity
  attributes do
    attribute :id,             Types::Int
    attribute :original_url,   Types::String
    attribute :target_url,     Types::String
    attribute :redirect_count, Types::Int
    attribute :created_at,     Types::Time
    attribute :updated_at,     Types::Time
  end
end
