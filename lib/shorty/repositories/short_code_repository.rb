class ShortCodeRepository < Hanami::Repository
  def find_by_target_url(url)
    short_codes.where(target_url: url).first
  end
end
